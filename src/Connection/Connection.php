<?php

namespace App\Connection;

use Exception;
use PDO;


class Connection
{
    public $db;
    private $host='localhost';
	private $databaseName='Security';
	private $user='simplon';
	private $password='1234';


    public function __construct()
    {
        try {
            $this->db = new PDO('mysql:host='.$this->host.';dbname='.$this->databaseName, $this->user, $this->password
            );
        } catch (Exception $e) {
            var_dump('Couldn\'t Establish A Database Connection. Due to the following reason: ' . $e->getMessage());
        }
    }
}
