<?php

namespace App\DAO;

use App\Connection\Connection;
use App\Entity\User;
use PDO;

class RegisterDAO extends Connection
{
  private $encryptedPassword;

  public function Register(User $user)
  {
    // $this->saltedPassword = str_shuffle($this->saltKey);
    $this->encryptedPassword = openssl_encrypt($user->getPassword(), "AES-128-ECB", $user->getSalt());
    $hashedPassword = hash('sha512', $this->encryptedPassword);
    $query = $this->db->prepare('INSERT INTO User (username, password, salt) VALUES (:username,:password,:salt)');
    $query->bindValue('username', $user->getUsername(), PDO::PARAM_STR);
    $query->bindValue('password', $hashedPassword, PDO::PARAM_STR);
    $query->bindValue('salt', $user->getSalt(), PDO::PARAM_STR);
    $query->execute();
  }
}
