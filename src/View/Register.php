<!DOCTYPE html>
<html>
<head>
  <title>Register</title
</head>
<body>
  <div class="header">
  	<h2>Register</h2>
  </div>
	
  <form method="post" action="">
  	<div class="input-group">
  	  <label>Username</label>
  	  <input type="email" name="username">
  	</div>
  	<div class="input-group">
  	  <label >Password</label>
  	  <input type="password" name="password">
  	</div>
  	<div class="input-group">
  	  <label >Confirm password</label>
  	  <input type="password" name="confirmed-password">
  	</div>
  	<div class="input-group">
  	  <button type="submit" class="btn" name="reg_user">Register</button>
  	</div>
  	<p>
  		Already a member? <a href="Login.php">Sign in</a>
  	</p>
  </form>
</body>
</html>