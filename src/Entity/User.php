<?php

namespace App\Entity;

class User {
    private $id;
    private $username;
    private $password;
    private $salt;

    public function __construct(string $username, string $password, string $salt,int $id = null) {
        $this->id = $id;
        $this->username = $username;
        $this->password= $password;
        $this->salt = $salt;
    }

    //Getter
    public function getUsername():string {
        return $this->username;
    }

    public function getPassword():string {
        return $this->password;
    }

    public function getId():int {
        return $this->id;
    }

    public function getSalt():string {
        return $this->salt;
    }


    //Setter
    public function setUsername(string $username): void {
        $this->username = $username;
    }

    public function setPassword(string $password): void {
        $this->password = $password;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }
    public function setSalt(string $salt): void {
        $this->salt = $salt;
    }
}
